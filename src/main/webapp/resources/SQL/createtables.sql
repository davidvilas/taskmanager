CREATE DATABASE task_manager;
USE task_manager;
CREATE TABLE task (
task_id INT AUTO_INCREMENT,
task_name VARCHAR(255),
task_done BOOLEAN,
PRIMARY KEY (task_id)
);