package imatia.taskmanager.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import imatia.taskmanager.model.entity.task.Task;
import imatia.taskmanager.model.entity.task.TaskBlock;
import imatia.taskmanager.model.entity.task.dao.TaskDAO;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskDAO taskDAO;

	static final int N_ELEMENTS_PAGE = 5;

	@Transactional
	public void createTask(String taskName) {
		taskDAO.save(new Task(taskName, false));
	}

	@Transactional
	public void completeTask(int taskId) {
		Task task = taskDAO.get(taskId);
		task.setTaskDone(true);
		taskDAO.save(task);
	}

	@Transactional
	public TaskBlock getTasksByCompletion(boolean done, int page) {
		int fResult = (page - 1) * N_ELEMENTS_PAGE;
		int maxResult = N_ELEMENTS_PAGE + 1;
		List<Task> tasks;
		if (done) {
			tasks = taskDAO.getDoneTasks(fResult, maxResult);
		} else {
			tasks = taskDAO.getUndoneTasks(fResult, maxResult);
		}
		if (tasks.size() > N_ELEMENTS_PAGE) {
			tasks.remove(N_ELEMENTS_PAGE);
			return new TaskBlock(tasks, true);
		} else {
			return new TaskBlock(tasks, false);
		}
	}

	@Transactional
	public void deleteTask(int taskId) {
		taskDAO.delete(taskId);
	}

}
