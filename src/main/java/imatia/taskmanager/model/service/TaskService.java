package imatia.taskmanager.model.service;

import imatia.taskmanager.model.entity.task.TaskBlock;

public interface TaskService {

	void createTask(String taskName);

	void completeTask(int taskId);

	void deleteTask(int taskId);

	public TaskBlock getTasksByCompletion(boolean completed, int page);
}
