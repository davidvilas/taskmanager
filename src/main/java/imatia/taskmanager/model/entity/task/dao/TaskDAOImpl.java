package imatia.taskmanager.model.entity.task.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import imatia.taskmanager.model.entity.task.Task;

@Repository
public class TaskDAOImpl implements TaskDAO {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(Task task) {
		getSession().saveOrUpdate(task);

	}

	public Task get(int taskId) {
		return (Task) getSession().createQuery("SELECT t FROM Task t WHERE t.taskId=:taskId")
				.setParameter("taskId", taskId).uniqueResult();
	}

	public void delete(int taskId) {
		Task deletedTask = getSession().get(Task.class, taskId);
		getSession().delete(deletedTask);

	}

	@SuppressWarnings("unchecked")
	public List<Task> getUndoneTasks(int fResult, int maxResult) {
		return (List<Task>) getSession()
				.createQuery("SELECT t FROM Task t WHERE t.taskDone=false ORDER BY t.taskId DESC")
				.setFirstResult(fResult).setMaxResults(maxResult).list();
	}

	@SuppressWarnings("unchecked")
	public List<Task> getDoneTasks(int fResult, int maxResult) {
		return (List<Task>) getSession()
				.createQuery("SELECT t FROM Task t WHERE t.taskDone=true ORDER BY t.taskId DESC")
				.setFirstResult(fResult).setMaxResults(maxResult).list();

	}

}
