package imatia.taskmanager.model.entity.task;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class Task {
	@Id
	@Column(name = "task_id")
	private int taskId;
	@Column(name = "task_name")
	private String taskName;
	@Column(name = "task_done")
	private boolean taskDone;

	public Task(String taskName, boolean taskDone) {
		super();
		this.taskName = taskName;
		this.taskDone = taskDone;
	}

	public Task() {
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public boolean isTaskDone() {
		return taskDone;
	}

	public void setTaskDone(boolean taskDone) {
		this.taskDone = taskDone;
	}

}
