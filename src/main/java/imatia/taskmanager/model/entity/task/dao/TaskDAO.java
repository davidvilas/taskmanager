package imatia.taskmanager.model.entity.task.dao;

import java.util.List;

import imatia.taskmanager.model.entity.task.Task;

public interface TaskDAO {

	void save(Task task);

	Task get(int taskId);

	List<Task> getUndoneTasks(int fResult, int maxResult);

	List<Task> getDoneTasks(int fResult, int maxResult);

	void delete(int taskId);
}
