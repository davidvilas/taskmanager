package imatia.taskmanager.model.entity.task;

import java.util.List;

public class TaskBlock {
	private List<Task> tasks;
	private boolean moreElements;

	public TaskBlock(List<Task> tasks, boolean moreElements) {
		super();
		this.tasks = tasks;
		this.moreElements = moreElements;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public boolean isMoreElements() {
		return moreElements;
	}

	public void setMoreElements(boolean moreElements) {
		this.moreElements = moreElements;
	}

}
