package imatia.taskmanager.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import imatia.taskmanager.model.entity.task.Task;
import imatia.taskmanager.model.entity.task.TaskBlock;
import imatia.taskmanager.model.service.TaskService;

@Controller
public class MainController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "redirect:/index?pageUndone=1&pageDone=1";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView main(@RequestParam("pageUndone") int pageUndone, @RequestParam("pageDone") int pageDone) {
		ModelAndView mav = new ModelAndView();
		TaskBlock undoneBlock = taskService.getTasksByCompletion(false, pageUndone);
		TaskBlock doneBlock = taskService.getTasksByCompletion(true, pageDone);
		mav.addObject("pageUndone", pageUndone);
		mav.addObject("pageDone", pageDone);
		mav.addObject("moreElementsDone", doneBlock.isMoreElements());
		mav.addObject("moreElementsUndone", undoneBlock.isMoreElements());
		mav.addObject("undoneTaskList", undoneBlock.getTasks());
		mav.addObject("doneTaskList", doneBlock.getTasks());
		mav.addObject(new Task());
		mav.setViewName("index");
		return mav;
	}

	@RequestMapping(value = "/completeTask", method = RequestMethod.GET)
	public String completeTask(@RequestParam("taskId") int taskId) {
		taskService.completeTask(taskId);
		return "redirect:/";
	}

	@RequestMapping(value = "/deleteTask", method = RequestMethod.GET)
	public String deleteTask(@RequestParam("taskId") int taskId) {
		taskService.deleteTask(taskId);
		return "redirect:/";
	}

	@RequestMapping(value = "/addTask", method = RequestMethod.POST)
	public String addTask(@ModelAttribute("task") Task task) {
		taskService.createTask(task.getTaskName());
		return "redirect:/";
	}
}
